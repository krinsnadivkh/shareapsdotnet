﻿using Microsoft.AspNetCore.Mvc;

namespace MvcMovicet.Controllers
{
    public class AR : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult E2()
        {
            return View();
        }
        public IActionResult ControllerPassDataToView(int id, string name)
        {
            ViewData["StuId"] = id;
            ViewData["StuName"] = name;
            ViewBag.StuId = id;
            ViewBag.StuName = name;
            return View();

        }
    }
}
